Mark Moreno
maalmore@ucsc.edu

class: cse130
project: asgn0
name: dog

files:
    dog.c
    Makefile
    README.md
    DESIGN.pdf
    WRITEUP.pdf

dog.c takes file.name arguments and outputs their contents. This program is 
similar to cat with the key difference that the files are outputted in the 
reverse order that they were entered. for example:
>>./cat file1 file2 file3 ->will be printed in the order file3 file2 file1
using a dash "-" for a file.name will read standard input from the command line
for example:
>>./cat file1 -
>> "your input"
will print:
>> "your input"
>> "<contents of file1>

*NOTE* My DESIGN.pdf will differ from the design of my actuall program because
I did not understand the instructions correctly until I started programming it.
At first I believed that the dash "-" would output the files in order 
for example:
>>./cat file1 file2 -> would be printed as file1 file2 instead of file2 file1