output: dog.o 
	gcc dog.o -o dog

dog.o: dog.c
	gcc -c dog.c

clean:
	rm *.o dog